FROM node:14.18.1-alpine

LABEL maintainer="enzogeron@gmail.com"

WORKDIR /var/www/vivatranslate

COPY package.json ./

RUN npm install

COPY .eslintrc.js nest-cli.json tsconfig.json tsconfig.build.json ./

COPY .env /var/www/vivatranslate/.env

CMD ["npm", "run", "start:dev", "--preserveWatchOutput"]
