import { registerAs } from '@nestjs/config'

export default registerAs('config', () => {
  return {
    environment: {
      name: process.env.NODE_ENV,
      port: process.env.PORT,
    },
  }
})
