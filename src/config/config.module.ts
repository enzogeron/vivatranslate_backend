import { ConfigModule } from '@nestjs/config'

import { environments } from './environments'
import configSchema from './configSchema'
import config from './config'

export default ConfigModule.forRoot({
  envFilePath: environments[process.env.NODE_ENV] || '.env',
  load: [config],
  isGlobal: true,
  validationSchema: configSchema,
})
