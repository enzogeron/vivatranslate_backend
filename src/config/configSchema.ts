import * as Joi from 'joi'

const configSchema = Joi.object({
  NODE_ENV: Joi.string().required(),
  PORT: Joi.number().required(),
})

export default configSchema
