import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { Document } from 'mongoose'
import { Question } from '../interfaces/questionary.interface'

export type QuestionaryDocument = Questionary & Document

@Schema()
export class Questionary {
  @Prop()
  title: string

  @Prop()
  email: string

  @Prop()
  questions: Question[]
}

export const QuestionarySchema = SchemaFactory.createForClass(Questionary)
