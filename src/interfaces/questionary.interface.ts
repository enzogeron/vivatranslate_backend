export interface Option {
  title: string
  isCorrect: boolean
}

export interface Question {
  title: string
  options: Option[]
}
