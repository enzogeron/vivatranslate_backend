import { HttpException, HttpStatus, Injectable } from '@nestjs/common'
import { InjectModel } from '@nestjs/mongoose'
import { Model } from 'mongoose'
import { LoginDTO } from '../auth/dtos/login.dto'
import { RegisterDTO } from './dtos/register.dto'
import { User, UserDocument } from 'src/models/user.schema'
import { Payload } from '../../types/payload'
import * as bcrypt from 'bcrypt'

@Injectable()
export class UserService {
  constructor(@InjectModel(User.name) private userModel: Model<UserDocument>) {}

  async create(registerDto: RegisterDTO) {
    const { email } = registerDto
    const user = await this.userModel.findOne({ email })
    if (user) {
      throw new HttpException('User already exists', HttpStatus.BAD_REQUEST)
    }

    const createdUser = new this.userModel(registerDto)
    await createdUser.save()
    return this.sanitizeUser(createdUser)
  }

  async validateLogin(loginDto: LoginDTO) {
    const { email, password } = loginDto
    const user = await this.userModel.findOne({ email })

    if (!user) {
      throw new HttpException('User doesnt exists', HttpStatus.BAD_REQUEST)
    }

    if (await bcrypt.compare(password, user.password)) {
      return this.sanitizeUser(user)
    } else {
      throw new HttpException('Invalid credentials', HttpStatus.BAD_REQUEST)
    }
  }

  async validatePayload(payload: Payload) {
    const { email } = payload
    return this.userModel.findOne({ email })
  }

  private sanitizeUser(user: User) {
    const sanitize = user
    return { email: sanitize.email }
  }
}
