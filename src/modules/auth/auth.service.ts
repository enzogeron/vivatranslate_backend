import { Injectable } from '@nestjs/common'
import { sign } from 'jsonwebtoken'
import { UserService } from '../user/user.service'
import { Payload } from '../../types/payload'

@Injectable()
export class AuthService {
  constructor(private userService: UserService) {}

  async signPayload(payload: Payload) {
    return sign(payload, process.env.SECRET_KEY, { expiresIn: '7d' })
  }

  async validateUser(payload: Payload) {
    return this.userService.validatePayload(payload)
  }
}
