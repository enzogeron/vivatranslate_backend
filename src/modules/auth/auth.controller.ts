import { Body, Controller, Post } from '@nestjs/common'
import { ApiOperation, ApiTags } from '@nestjs/swagger'
import { RegisterDTO } from '../user/dtos/register.dto'
import { UserService } from '../user/user.service'
import { AuthService } from './auth.service'
import { LoginDTO } from './dtos/login.dto'

@ApiTags('Authentication')
@Controller('auth')
export class AuthController {
  constructor(
    private readonly authService: AuthService,
    private readonly userService: UserService,
  ) {}

  @Post('register')
  @ApiOperation({
    summary: 'Register',
  })
  async register(@Body() registerDto: RegisterDTO) {
    const user = await this.userService.create(registerDto)
    const payload = {
      email: user.email,
    }
    const token = await this.authService.signPayload(payload)
    return { user, token }
  }

  @Post('login')
  @ApiOperation({
    summary: 'Login',
  })
  async login(@Body() loginDto: LoginDTO) {
    const user = await this.userService.validateLogin(loginDto)
    const payload = {
      email: user.email,
    }

    const token = await this.authService.signPayload(payload)
    return { user, token }
  }
}
