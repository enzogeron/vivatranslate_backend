import { BadRequestException, Injectable } from '@nestjs/common'
import { InjectModel } from '@nestjs/mongoose'
import { Model } from 'mongoose'
import { CreateQuestionaryDTO } from './dtos/createQuestionary.dto'
import { UpdateQuestionaryDTO } from './dtos/updateQuestionary.dto'
import {
  Questionary,
  QuestionaryDocument,
} from '../../models/questionary.schema'

@Injectable()
export class QuestionaryService {
  constructor(
    @InjectModel(Questionary.name)
    private readonly questionaryModel: Model<QuestionaryDocument>,
  ) {}

  async create(email: string, createQuestionaryDto: CreateQuestionaryDTO) {
    const newQuestionary = new this.questionaryModel({
      email,
      ...createQuestionaryDto,
    })
    return newQuestionary.save()
  }

  async edit(
    idQuestionary: string,
    email: string,
    updateQuestionaryDto: UpdateQuestionaryDTO,
  ) {
    const validate = await this.validateQuestionaryUser(idQuestionary, email)
    if (!validate) {
      throw new BadRequestException('There is no questionnaire')
    }
    return this.questionaryModel.findByIdAndUpdate(
      idQuestionary,
      updateQuestionaryDto,
      { new: true },
    )
  }

  async delete(idQuestionary: string, email: string) {
    const validate = await this.validateQuestionaryUser(idQuestionary, email)
    if (!validate) {
      throw new BadRequestException('There is no questionnaire')
    }
    return this.questionaryModel.findByIdAndRemove(idQuestionary)
  }

  async find() {
    return this.questionaryModel.find()
  }

  async findByEmail(email: string) {
    return this.questionaryModel.find({ email })
  }

  async findById(idQuestionary: string) {
    return this.questionaryModel.findById(idQuestionary)
  }

  private async validateQuestionaryUser(idQuestionary: string, email: string) {
    const questionary = await this.findById(idQuestionary)
    return questionary.email === email
  }
}
