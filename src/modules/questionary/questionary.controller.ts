import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  UseGuards,
} from '@nestjs/common'
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger'
import { JwtAuthGuard } from '../auth/jwt-auth.guard'
import { CreateQuestionaryDTO } from './dtos/createQuestionary.dto'
import { UpdateQuestionaryDTO } from './dtos/updateQuestionary.dto'
import { QuestionaryService } from './questionary.service'
import { AuthUser } from '../../decorators/user.decorator'

@ApiTags('Questionaries')
@Controller('questionary')
export class QuestionaryController {
  constructor(private readonly questionaryService: QuestionaryService) {}

  @Post('create')
  @UseGuards(JwtAuthGuard)
  @ApiOperation({
    summary: 'Create questionary',
  })
  @ApiBearerAuth()
  async create(
    @AuthUser() user,
    @Body() createQuestionaryDto: CreateQuestionaryDTO,
  ) {
    return this.questionaryService.create(user.email, createQuestionaryDto)
  }

  @Put('edit/:id')
  @UseGuards(JwtAuthGuard)
  @ApiOperation({
    summary: 'Edit questionary by id',
  })
  @ApiBearerAuth()
  async edit(
    @AuthUser() user,
    @Param('id') id: string,
    @Body() updateQuestionaryDto: UpdateQuestionaryDTO,
  ) {
    return this.questionaryService.edit(id, user.email, updateQuestionaryDto)
  }

  @Delete('delete/:id')
  @UseGuards(JwtAuthGuard)
  @ApiOperation({
    summary: 'Delete questionary by id',
  })
  @ApiBearerAuth()
  async delete(@AuthUser() user, @Param('id') id: string) {
    return this.questionaryService.delete(id, user.email)
  }

  @Get('me')
  @UseGuards(JwtAuthGuard)
  @ApiOperation({
    summary: 'Get all questionaries by user authenticated',
  })
  @ApiBearerAuth()
  async findByEmail(@AuthUser() user) {
    return this.questionaryService.findByEmail(user.email)
  }

  @Get()
  @ApiOperation({
    summary: 'Get all questionaries',
  })
  async find() {
    return this.questionaryService.find()
  }

  @Get(':id')
  @ApiOperation({
    summary: 'Get questionary by id',
  })
  async findById(@Param('id') id: string) {
    return this.questionaryService.findById(id)
  }
}
