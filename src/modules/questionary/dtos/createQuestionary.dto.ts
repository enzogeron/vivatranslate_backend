import { ApiProperty } from '@nestjs/swagger'
import { IsArray, IsNotEmpty, IsString } from 'class-validator'
import { Option, Question } from '../../../interfaces/questionary.interface'

class QuestionDTO implements Question {
  title: string
  options: Option[]
}
export class CreateQuestionaryDTO {
  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  title: string

  @IsNotEmpty()
  @IsArray()
  @ApiProperty({
    type: QuestionDTO,
    example: [
      {
        title: 'Answer 01',
        options: [
          {
            title: 'Option 1',
            isCorrect: true,
          },
          {
            title: 'Option 2',
            isCorrect: false,
          },
        ],
      },
      {
        title: 'Answer 02',
        options: [
          {
            title: 'Option 1',
            isCorrect: false,
          },
          {
            title: 'Option 2',
            isCorrect: true,
          },
        ],
      },
    ],
  })
  questions: QuestionDTO[]
}
