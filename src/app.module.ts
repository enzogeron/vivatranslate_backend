import { Module } from '@nestjs/common'
import { MongooseModule } from '@nestjs/mongoose'
import { UserModule } from './modules/user/user.module'
import { AuthModule } from './modules/auth/auth.module'
import { QuestionaryModule } from './modules/questionary/questionary.module'

import ConfigModule from './config/config.module'

const url = process.env.MONGODB_URL || 'localhost'

@Module({
  imports: [
    ConfigModule,
    MongooseModule.forRoot(process.env.MONGODB_URI),
    UserModule,
    QuestionaryModule,
    AuthModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
